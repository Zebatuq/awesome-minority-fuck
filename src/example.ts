import {exampleExportFunction} from './exampleLib';

let script: ScriptDescription = {};

script.OnUpdate = () => {
    console.log(exampleExportFunction());
};

RegisterScript(script);
